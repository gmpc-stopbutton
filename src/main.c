/* gmpc-stopbutton (GMPC plugin)
 * Copyright (C) 2006-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <glade/glade.h>
#include <gmpc/plugin.h>
#include <config.h>
/* External pointer + function, there internal from gmpc */
extern GladeXML *pl3_xml;
int stop_song();
GtkWidget *stopbutton = NULL;

static void stop_button_add() {
	if(pl3_xml == NULL) return;
	stopbutton = gtk_button_new();
	gtk_container_add(GTK_CONTAINER(stopbutton), gtk_image_new_from_stock("gtk-media-stop", GTK_ICON_SIZE_BUTTON));
	g_signal_connect(G_OBJECT(stopbutton), "clicked",G_CALLBACK(stop_song), NULL); 
	gtk_box_pack_start(GTK_BOX(glade_xml_get_widget(pl3_xml, "hbox12")), stopbutton, FALSE, TRUE,0);
	gtk_box_reorder_child(GTK_BOX(glade_xml_get_widget(pl3_xml, "hbox12")), stopbutton, 2);
	gtk_widget_show_all(stopbutton);
	cfg_set_single_value_as_int(config,"stopbutton", "enabled", 1);
}
static void stop_button_init() {
	if(	cfg_get_single_value_as_int_with_default(config,"stopbutton", "enabled", 1))
	{
		gtk_init_add((GtkFunction )stop_button_add, NULL);
	}
}
static int get_enabled() {
	if(stopbutton) return TRUE;
	return FALSE;	
}
static void set_enabled() {
	if(stopbutton == NULL) {
		stop_button_add();
	}
	else {
		cfg_set_single_value_as_int(config,"stopbutton", "enabled", 0);
		gtk_widget_destroy(stopbutton);
		stopbutton = NULL;
	}
}
gmpcPlugin plugin = {
	"Stop Button Add",
	{PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
	GMPC_PLUGIN_NO_GUI,
	0,  	/* plugin id */
	NULL,   /* path to plugin */
	stop_button_init,   /* initialization */
        NULL,   /* destroy */
	NULL, /* browser intergration */
	NULL,	/* status changed */
	NULL,	/* connection changed */
	NULL,	/* preferences */
	NULL,	/* MetaData */ 
	get_enabled,
	set_enabled
};
int plugin_api_version = PLUGIN_API_VERSION;
